/**
 * Created by andreimirica on 27.04.2016.
 */

var async = require('async');
var Q = require('q');
var request = require('request');
var Config = require('../../config/environment.js');
var config = new Config();
var translate = require('yandex-translate')(config.yandexKey);
var alchemyPrefix = 'http://gateway-a.watsonplatform.net/calls/text/TextGetEmotion';
var detect = require('detect-gender');
var supportedLanguages = [
    'Afrikaans',
    'Albanian',
    'Arabic',
    'Armenian',
    'Azerbaijani',
    'Basque',
    'Belarusian',
    'Bulgarian',
    'Catalan',
    'Chinese',
    'Croatian',
    'Czech',
    'Danish',
    'Dutch',
    'English',
    'Estonian',
    'Filipino',
    'Finnish',
    'French',
    'Galician',
    'Georgian',
    'German',
    'Greek',
    'Haitian Creole',
    'Hebrew',
    'Hindi',
    'Hungarian',
    'Icelandic',
    'Indonesian',
    'Irish',
    'Italian',
    'Japanese',
    'Korean',
    'Latvian',
    'Lithuanian',
    'Macedonian',
    'Malay',
    'Maltese',
    'Norwegian',
    'Persian',
    'Polish',
    'Portuguese',
    'Romanian',
    'Russian',
    'Serbian',
    'Slovak',
    'Slovenian',
    'Spanish',
    'Swahili',
    'Swedish',
    'Thai',
    'Turkish',
    'Ukrainian',
    'Urdu',
    'Vietnamese',
    'Welsh',
    'Yiddish'
];

exports.fullAnalysisSocialProfiles = function fullAnalysisSocialProfiles(objectWithAllProfiles, alchemy){
    var deferred = Q.defer();
    async.each(Object.keys(objectWithAllProfiles), function(profile, callback){
        switch(profile){
            case 'twitter':
               async.each(objectWithAllProfiles[profile], function (singleProfile, callbackT) {
                  async.each(singleProfile.newsFeed, function (singleFeed, callbackFeed) {
                      //first we'll detect the language of the text, then translate it and then we'll apply
                      // the alchemy api to analyze the feelings
                      translate.translate(singleFeed.text, { to: 'en' }, function(err, res) {
                          if(err){
                              //if it can't be translated to english for text analysis, then we'll create an object
                              //similar to the one received using alchemy
                              singleFeed.emotionAnalysis = {
                                  "status": 200,
                                  "docEmotions": {
                                      "anger": 0,
                                      "disgust": 0,
                                      "fear": 0,
                                      "joy": 0,
                                      "sadness": 0
                                  }
                              };
                              callbackFeed();
                          } else {
                              singleFeed.translatedText = res.text;
                              var propertiesObject = {
                                  apikey: config.alchemyKey,
                                  text: singleFeed.translatedText,
                                  outputMode: 'json'
                              };
                              var options = {
                                  url: alchemyPrefix,
                                  headers: {
                                      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'
                                  },
                                  qs: propertiesObject
                              };
                              request(options, function (error, response, body) {
                                  if (!error && response.statusCode == 200) {
                                      if(body.status == 200){
                                          singleFeed.emotionAnalysis = body;
                                          callbackFeed();
                                      } else {
                                          callbackFeed(body.status);
                                      }
                                  }
                              })
                          }
                      });
                  }, function (err) {
                    if (err){
                        callbackT(err);
                    } else {
                        callbackT();
                    }
                  }) 
               }, function (err) {
                   if (err){
                       callback(err);
                   } else {
                       callback();
                   }
               });
                break;
            case 'linkedincompany':
                async.each(objectWithAllProfiles[profile], function (singleProfile, callbackL) {
                    async.each(singleProfile.newsFeed.linkedinFeed.updates, function (singleFeed, callbackFeed) {
                        //first we'll detect the language of the text, then translate it and then we'll apply
                        // the alchemy api to analyze the feelings
                        translate.translate(singleFeed.text, { to: 'en' }, function(err, res) {
                            if(err){
                                //if it can't be translated to english for text analysis, then we'll create an object
                                //similar to the one received using alchemy
                                singleFeed.emotionAnalysis = {
                                    "status": 200,
                                    "docEmotions": {
                                        "anger": 0,
                                        "disgust": 0,
                                        "fear": 0,
                                        "joy": 0,
                                        "sadness": 0
                                    }
                                };
                                callbackFeed();
                            } else {
                                singleFeed.translatedText = res.text;
                                var propertiesObject = {
                                    apikey: config.alchemyKey,
                                    text: singleFeed.translatedText,
                                    outputMode: 'json'
                                };
                                var options = {
                                    url: alchemyPrefix,
                                    headers: {
                                        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'
                                    },
                                    qs: propertiesObject
                                };
                                request(options, function (error, response, body) {
                                    if (!error && response.statusCode == 200) {
                                        if(body.status == 200){
                                            singleFeed.emotionAnalysis = body;
                                            callbackFeed();
                                        } else {
                                            callbackFeed(body.status);
                                        }
                                    }
                                })
                            }
                        });
                    }, function (err) {
                        if (err){
                            callbackL(err);
                        } else {
                            callbackL();
                        }
                    })
                }, function (err) {
                    if (err){
                        callback(err);
                    } else {
                        callback();
                    }
                });
                break;
            case 'facebook':
                async.each(objectWithAllProfiles[profile], function (singleProfile, callbackF) {
                    async.each(singleProfile.newsFeed, function (singleFeed, callbackFeed) {
                        //first we'll detect the language of the text, then translate it and then we'll apply
                        // the alchemy api to analyze the feelings
                        if(!singleFeed.message){
                            singleFeed.emotionAnalysis = {
                                "status": 200,
                                "docEmotions": {
                                    "anger": 0,
                                    "disgust": 0,
                                    "fear": 0,
                                    "joy": 0,
                                    "sadness": 0
                                }
                            };
                            callbackFeed();
                        } else {
                            translate.translate(singleFeed.message, { to: 'en' }, function(err, res) {
                                if(err){
                                    //if it can't be translated to english for text analysis, then we'll create an object
                                    //similar to the one received using alchemy
                                    singleFeed.emotionAnalysis = {
                                        "status": 200,
                                        "docEmotions": {
                                            "anger": 0,
                                            "disgust": 0,
                                            "fear": 0,
                                            "joy": 0,
                                            "sadness": 0
                                        }
                                    };
                                    callbackFeed();
                                } else {
                                    singleFeed.translatedText = res.text;
                                    var propertiesObject = {
                                        apikey: config.alchemyKey,
                                        text: singleFeed.translatedText,
                                        outputMode: 'json'
                                    };
                                    var options = {
                                        url: alchemyPrefix,
                                        headers: {
                                            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'
                                        },
                                        qs: propertiesObject
                                    };
                                    request(options, function (error, response, body) {
                                        if (!error && response.statusCode == 200) {
                                            if(body.status == 200){
                                                singleFeed.emotionAnalysis = body;
                                                callbackFeed();
                                            } else {
                                                callbackFeed(body.status);
                                            }
                                        }
                                    })
                                }
                            });
                        }
                    }, function (err) {
                        if (err){
                            callbackF(err);
                        } else {
                            callbackF();
                        }
                    })
                }, function (err) {
                    if (err){
                        callback(err);
                    } else {
                        callback();
                    }
                });
                break;
            case 'Indeed':
                async.each(objectWithAllProfiles[profile], function (singleProfile, callbackI) {
                    async.each(singleProfile.newsFeed, function (singleFeed, callbackFeed) {
                        //first we'll detect the language of the text, then translate it and then we'll apply
                        // the alchemy api to analyze the feelings
                        var stringToAnalyze = singleFeed.title + ' ' + singleFeed.content_description + ' ' + singleFeed.review_pros + ' ' + singleFeed.content_cons
                        translate.translate(stringToAnalyze, { to: 'en' }, function(err, res) {
                            if(err){
                                //if it can't be translated to english for text analysis, then we'll create an object
                                //similar to the one received using alchemy
                                singleFeed.emotionAnalysis = {
                                    "status": 200,
                                    "docEmotions": {
                                        "anger": 0,
                                        "disgust": 0,
                                        "fear": 0,
                                        "joy": 0,
                                        "sadness": 0
                                    }
                                };
                                callbackFeed();
                            } else {
                                singleFeed.translatedText = res.text;
                                var propertiesObject = {
                                    apikey: config.alchemyKey,
                                    text: singleFeed.translatedText,
                                    outputMode: 'json'
                                };
                                var options = {
                                    url: alchemyPrefix,
                                    headers: {
                                        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'
                                    },
                                    qs: propertiesObject
                                };
                                request(options, function (error, response, body) {
                                    if (!error && response.statusCode == 200) {
                                        if(body.status == 200){
                                            singleFeed.emotionAnalysis = body;
                                            callbackFeed();
                                        } else {
                                            callbackFeed(body.status);
                                        }
                                    }
                                })
                            }
                        });
                    }, function (err) {
                        if (err){
                            callbackI(err);
                        } else {
                            callbackI();
                        }
                    })
                }, function (err) {
                    if (err){
                        callback(err);
                    } else {
                        callback();
                    }
                });
                break;
            default:
                callback();
        }
    },function (err) {
        if (err){
            deferred.reject(err);
        } else {
            deferred.resolve(objectWithAllProfiles);
        }
    });
    return deferred.promise;
};

exports.fullAnalysisSingleSocialProfile = function fullAnalysisSingleSocialProfile(arrayOfPosts, nameOfSocialProfile, alchemy){
    var deferred = Q.defer();
    async.each(arrayOfPosts, function (singleFeed, callbackFeed) {
        //first we'll detect the language of the text, then translate it and then we'll apply
        // the alchemy api to analyze the
        var stringToAnalyze = '';
        switch (nameOfSocialProfile) {
            case 'twitter' :
                stringToAnalyze = singleFeed.text ? singleFeed.text : null;
                break;
            case 'facebook' :
                stringToAnalyze = singleFeed.message ? singleFeed.message : null;
                break;
            case 'linkedincompany' :
                stringToAnalyze = singleFeed.text ? singleFeed.text : null;
                break;
            case 'Indeed' :
                stringToAnalyze = singleFeed.title + ' ' + singleFeed.content_description + ' ' + singleFeed.review_pros + ' ' + singleFeed.content_cons
                break;
        }
        if(!stringToAnalyze){
            singleFeed.emotionAnalysis = {
                "status": 200,
                "docEmotions": {
                    "anger": 0,
                    "disgust": 0,
                    "fear": 0,
                    "joy": 0,
                    "sadness": 0
                }
            };
            callbackFeed();
        } else {
            translate.translate(stringToAnalyze, { to: 'en' }, function(err, res) {
                if(err){
                    //if it can't be translated to english for text analysis, then we'll create an object
                    //similar to the one received using alchemy
                    singleFeed.emotionAnalysis = {
                        "status": 200,
                        "docEmotions": {
                            "anger": 0,
                            "disgust": 0,
                            "fear": 0,
                            "joy": 0,
                            "sadness": 0
                        }
                    };
                    callbackFeed();
                } else {
                    singleFeed.translatedText = res.text;
                    var propertiesObject = {
                        apikey: config.alchemyKey,
                        text: singleFeed.translatedText,
                        outputMode: 'json'
                    };
                    var options = {
                        url: alchemyPrefix,
                        headers: {
                            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'
                        },
                        qs: propertiesObject
                    };
                    request(options, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            if(body.status == 200){
                                singleFeed.emotionAnalysis = body;
                                callbackFeed();
                            } else {
                                callbackFeed(body.status);
                            }
                        }
                    })
                }
            });
        }
    }, function (err) {
        if (err){
            deferred.reject(err);
        } else {
            deferred.resolve(arrayOfPosts);
        }
    });
    return deferred.promise;
};

exports.commentsAnalysis = function commentsAnalysis(arrayOfComments, nameOfSocialProfile, alchemy){
    var deferred = Q.defer();
    async.each(arrayOfComments, function (singleComment, callbackFeed) {
        //first we'll detect the language of the text, then translate it and then we'll apply
        // the alchemy api to analyze the
        var stringToAnalyze = '';
        switch (nameOfSocialProfile) {
            case 'twitter' :
                stringToAnalyze = singleComment.text ? singleComment.text : null;
                break;
            case 'facebook' :
                stringToAnalyze = singleComment.message ? singleComment.message : null;
                break;
            case 'linkedincompany' :
                stringToAnalyze = singleComment.text ? singleComment.text : null;
                break;
        }
        if(!stringToAnalyze){
            singleComment.emotionAnalysis = {
                "status": 200,
                "docEmotions": {
                    "anger": 0,
                    "disgust": 0,
                    "fear": 0,
                    "joy": 0,
                    "sadness": 0
                }
            };
            callbackFeed();
        } else {
            translate.translate(stringToAnalyze, { to: 'en' }, function(err, res) {
                if(err){
                    //if it can't be translated to english for text analysis, then we'll create an object
                    //similar to the one received using alchemy
                    singleFeed.emotionAnalysis = {
                        "status": 200,
                        "docEmotions": {
                            "anger": 0,
                            "disgust": 0,
                            "fear": 0,
                            "joy": 0,
                            "sadness": 0
                        }
                    };
                    callbackFeed();
                } else {
                    singleComment.translatedText = res.text;
                    var propertiesObject = {
                        apikey: config.alchemyKey,
                        text: singleComment.translatedText,
                        outputMode: 'json'
                    };
                    var options = {
                        url: alchemyPrefix,
                        headers: {
                            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'
                        },
                        qs: propertiesObject
                    };
                    request(options, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            if(body.status == 200){
                                singleComment.emotionAnalysis = body;
                                callbackFeed();
                            } else {
                                callbackFeed(body.status);
                            }
                        }
                    })
                }
            });
        }
    }, function (err) {
        if (err){
            deferred.reject(err);
        } else {
            deferred.resolve(arrayOfComments);
        }
    });
    return deferred.promise;
};

exports.getGender = function getGender (arrayOfComments, socialProfileName){
    var deferred = Q.defer();
    async.each(arrayOfComments, function (singleComment, callbackFeed) {
        //first we'll detect the language of the text, then translate it and then we'll apply
        // the alchemy api to analyze the
        var stringToAnalyze = '';
        switch (socialProfileName) {
            case 'twitter' :
                stringToAnalyze = singleComment.fullName ? singleComment.fullName : null;
                break;
            case 'facebook' :
                stringToAnalyze = singleComment.from ? singleComment.from.name : null;
                break;
            case 'linkedincompany' :
                stringToAnalyze = singleComment.fullName ? singleComment.fullName : null;
                break;
        }
        if(!stringToAnalyze){
            singleComment.gender = 'Unknown';
            callbackFeed();
        } else {
            detect(stringToAnalyze).then(function (gender) {
                if(!gender){
                    singleComment.gender = 'Unknown';
                    callbackFeed();
                } else {
                    singleComment.gender = gender;
                    callbackFeed();
                }
            });
        }
    }, function (err) {
        if (err){
            deferred.reject(err);
        } else {
            deferred.resolve(arrayOfComments);
        }
    });
    return deferred.promise;
};