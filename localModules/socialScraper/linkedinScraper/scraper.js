/**
 * Created by Administrator on 05/04/16.
 */
var request = require('request');
var cheerio = require('cheerio');
var Q = require('q');
var _ = require('underscore');
var moreUpdatesURLPartOne = "https://www.linkedin.com/biz/";
var async = require('async');

exports.getReviewsFromLinkedinURL = getReviewsFromLinkedinURL = function getReviewsFromLinkedinURL(url, linkedinCompanyId) {
    var deferred = Q.defer();
    var options = {
        url: url,
        headers: {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            parseInitialBodyForLinkedinReviews(body, linkedinCompanyId).then(
                function(success){
                    deferred.resolve(success);
                },
                function(err){
                    deferred.reject(err);
                }
            );
        } else {
            deferred.reject('Could not get reviews from Linkedin!');
        }
    });
    return deferred.promise;
};

function newFilledArray(length) {
    var array = [];
    var i = 0;
    while (i < length) {
        array[i++] = i;
    }
    return array;
}

function parseInitialBodyForLinkedinReviews (body, linkedinCompanyId) {
    var $ = cheerio.load(body);
    var deferred = Q.defer();
    var data = {
        linkedinData: ""
    };
    data.linkedinData = $('#stream-feed-embed-id-content').html();
    data.linkedinData = data.linkedinData.replace('<!--', '').replace('-->', '');
    data.linkedinData = JSON.parse(data.linkedinData);
    data.linkedinFeed = data.linkedinData.feed;
    var totalUpdates = data.linkedinFeed.total;
    var numPages = Math.ceil(totalUpdates/10);
    var arrayToIterate = newFilledArray(numPages);
    transformInitialFeed(data.linkedinFeed.updates).then(
        function(success){
            data.linkedinFeed.updates = success;
            async.forEachSeries(arrayToIterate, function(arrayItem, callback){
                var pageIndex = arrayItem * 10;
                if(arrayItem == numPages){
                    pageIndex = totalUpdates;
                }
                var searchURL = moreUpdatesURLPartOne + linkedinCompanyId + '/feed?start=' + pageIndex + '&v2=true';
                var updatesArray = [];
                var options = {
                    url: searchURL,
                    headers: {
                        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'
                    }
                };
                request(options, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var $ = cheerio.load(body);
                        $('.feed-content').each(function(j, item){
                            updateData 		= $(this);
                            updateComments = [];
                            updateText 		= updateData.find('.js-truncate-commentary').eq(0).text();
                            update_date 		= updateData.find('.nus-timestamp').text();
                            updateLikes = updateData.find('.unlike').find('span').text();
                            if(updateData.find('.comment-item').length > 0 ){
                                for(var i=0; i< updateData.find('.comment-item').length; i++) {
                                    var commentObject = {};
                                    commentObject.username = updateData.find('.comment-item').eq(i).find('a').eq(0).attr('href');
                                    commentObject.fullName = updateData.find('.comment-item').eq(i).find('.comment-details').eq(0).find('a').text();
                                    commentObject.text = updateData.find('.comment-item').eq(i).find('.comment-text').text();
                                    updateComments.push(commentObject);
                                }
                            }
                            var update = {
                                'text':updateText,
                                'date':update_date,
                                'likes':updateLikes,
                                'comments':updateComments
                            };
                            updatesArray.push(update);
                        });
                        data.linkedinFeed.updates = data.linkedinFeed.updates.concat(updatesArray);
                        callback();
                    } else {
                        console.log(response.statusCode);
                        console.log(error);
                        callback('Could not get reviews from Linkedin!');
                    }
                });
            }, function (err) {
                if(err){
                    deferred.reject(err);
                }else{
                    deferred.resolve(data);
                }
            });
        },
        function (err) {
            deferred.reject(err);
        }
    );

    return deferred.promise;
}

function transformInitialFeed (feedArray){
    var deferred = Q.defer();
    var arrayTransformed = [];
    async.each(feedArray, function(feedItem, callbackFeedItem){
        var itemToPush = {};
        for(var i = 0; i < feedItem.commentary.length; i++){
            itemToPush.text =  "";
            var textTransformed = feedItem.commentary[i].plain ? feedItem.commentary[i].plain.text + ' ' : feedItem.commentary[i].link.text + ' ';
            itemToPush.text += textTransformed;
        }
        itemToPush.date = feedItem.updateRelativeTime;
        if(feedItem.likes){
            for(property in feedItem.likes){
                if(feedItem.likes.hasOwnProperty(property)){
                    itemToPush.likes = Object.keys(feedItem.likes[property]).length;
                }
            }
        }
        itemToPush.comments = [];
        if(feedItem.comments){
            for(var k = 0 ; k < feedItem.comments.length; k++){
                var commentObject = {};
                commentObject.username = feedItem.comments[k].commenter.member ? feedItem.comments[k].commenter.member.url : feedItem.comments[k].commenter.company.url;
                commentObject.fullName = feedItem.comments[k].commenter.member ? feedItem.comments[k].commenter.member.name : feedItem.comments[k].commenter.company.name;
                for(var j = 0; j < feedItem.comments[k].commentary.length; j++){
                    commentObject.text =  "";
                    var textTransformed = feedItem.comments[k].commentary[j].plain ? feedItem.comments[k].commentary[j].plain.text + ' ' : feedItem.comments[k].commentary[j].link.text + ' ';
                    commentObject.text += textTransformed;
                }
                itemToPush.comments.push(commentObject);
            }
        }
        arrayTransformed.push(itemToPush);
        callbackFeedItem();
    }, function (err){
        if(err){
            deferred.reject(err);
        } else {
            deferred.resolve(arrayTransformed);
        }
    });

    return deferred.promise;
}