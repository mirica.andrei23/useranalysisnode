/**
 * Created by Administrator on 05/04/16.
 */
/**
 * Created by Administrator on 04/04/16.
 */
var linkedin 		= require('./scraper');
var Q 				= require('q');

exports.crawlLinkedinUpdates = function crawlLinkedinUpdates(linkedinURL, linkedinCompanyId) {
    var deferred = Q.defer();
    linkedin.getReviewsFromLinkedinURL(linkedinURL, linkedinCompanyId)
        .then(
        function (success) {
            deferred.resolve(success);
        },
        function (err) {
            deferred.reject(err);
        }
    );

    return deferred.promise;

};
