/**
 * Created by Administrator on 05/04/16.
 */
/**
 * Created by Administrator on 04/04/16.
 */
var twitter 		= require('./scraper');
var Q 				= require('q');
var TwitterPosts = require('../twitter-screen-scrape');

exports.crawlTwitterUpdates = function crawlTwitterUpdates(twitterUsername, twitterURL) {
    var deferred = Q.defer();
    var tweetsAssociated = [];
    streamOfTweets = new TwitterPosts({
        username: twitterUsername,
        retweets: true
    });
    streamOfTweets.on('readable', function() {
        var tweet;
        tweet = streamOfTweets.read();
        tweet.localTime = new Date(tweet.time * 1000);
        tweetsAssociated.push(tweet);
    }).on('end',function(){
        twitter.getUpdatesWithComments(tweetsAssociated, twitterURL)
            .then(
                function (success) {
                    deferred.resolve(success);
                },
                function (err) {
                    deferred.reject(err);
                }
            );
    });
    return deferred.promise;
};
