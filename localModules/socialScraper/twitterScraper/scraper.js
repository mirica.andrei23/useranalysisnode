/**
 * Created by Administrator on 05/04/16.
 */
var request = require('request');
var cheerio = require('cheerio');
var Q = require('q');
var _ = require('underscore');
var moreUpdatesURLPartOne = "/status/";
var async = require('async');
var twitterPrefix = 'https://twitter.com';
var rp = require('request-promise');

exports.getUpdatesWithComments = getUpdatesWithComments = function getUpdatesWithComments(tweets, twitterURL) {
    var deferred = Q.defer();
    var i = 0;
    var j = 0;
    async.each(tweets, function(tweet, callback){
        var options = {
            uri: twitterURL + moreUpdatesURLPartOne + tweet.id,
            headers: {
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'
            },
            timeout: 5 * 60 * 1000
        };
        i += 1;
        console.log('now is :' + i);
        request(options, function(err, response, body){
            if (!err && response.statusCode == 200) {
                j++;
                var $ = cheerio.load(body);
                var comments = [];
                if($('.ThreadedConversation-tweet').length > 0){
                    console.log('now is comments :' + j);
                    $('.ThreadedConversation-tweet').each(function(j, item){
                        commentData 		= $(this);
                        var comment = {};
                        if(commentData.next().hasClass('ThreadedConversation-viewOther')){
                            comment.text = commentData.find('.TweetTextSize').text();
                            comment.date = commentData.find('.tweet-timestamp').attr('title');
                            comment.username = twitterPrefix + commentData.find('.stream-item-header').find('a').attr('href');
                            comments.push(comment);
                            var urlForDetails = twitterPrefix + commentData.next().find('a').attr('href');
                            getCommentsAndReplies(urlForDetails).then(
                                function(success){
                                    comments = comments.concat(success);
                                },
                                function(err){
                                    callback(err);
                                }
                            )
                        } else {
                            comment.text = commentData.find('.TweetTextSize').text();
                            comment.date = commentData.find('.tweet-timestamp').attr('title');
                            comment.username = twitterPrefix + commentData.find('.stream-item-header').find('a').attr('href');
                            comment.fullName = commentData.find('.stream-item-header').find('a').find('.fullname').text();
                            comment.userID = commentData.find('.stream-item-header').find('a').attr('data-user-id');
                            comments.push(comment);
                        }
                    });
                    tweet.comments = comments;
                    callback();
                } else {
                    if($('.ThreadedConversation--loneTweet').length > 0){
                        var comment = {};
                        comment.text = $('.ThreadedConversation--loneTweet').find('.TweetTextSize').text();
                        comment.date = $('.ThreadedConversation--loneTweet').find('.tweet-timestamp').attr('title');
                        comment.username = twitterPrefix +  $('.ThreadedConversation--loneTweet').find('.stream-item-header').find('a').attr('href');
                        comment.fullName = $('.ThreadedConversation--loneTweet').find('.stream-item-header').find('a').find('.fullname').text();
                        comment.userID = $('.ThreadedConversation--loneTweet').find('.stream-item-header').find('a').attr('data-user-id');
                        tweet.comments = [];
                        tweet.comments.push(comment);
                        callback();
                    } else {
                        tweet.comments = [];
                        callback();
                    }
                }
            } else {
                callback();
                // deferred.reject('Could not get comments from Twitter!');
            }
        });
    },function (err) {
        if(err){
            deferred.reject(err);
        }else{
            deferred.resolve(tweets);
        }
    });
    return deferred.promise;
};

function getCommentsAndReplies (url) {
    var deferred = Q.defer();
    var newOptions = {
        uri: url,
        headers: {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'
        },
        timeout: 5 * 60 * 1000
    };
    var replies = [];
    var comment = {};
    request(newOptions, function(err, response, body){
                if (response.statusCode == 200) {
                    var $ = cheerio.load(body);
                    $('.ThreadedConversation-tweet').each(function(j, item){
                        commentData 		= $(this);
                        comment.text = commentData.find('.TweetTextSize').text();
                        comment.date = $(this).find('.tweet-timestamp').attr('title');
                        comment.username = twitterPrefix + $(this).find('.stream-item-header').find('a').attr('href');
                        replies.push(comment);
                    });
                    if($('.ThreadedConversation--loneTweet').length > 0){
                        comment.text = $('.ThreadedConversation--loneTweet').find('.TweetTextSize').text();
                        comment.date = $('.ThreadedConversation--loneTweet').find('.tweet-timestamp').attr('title');
                        comment.username = twitterPrefix +  $('.ThreadedConversation--loneTweet').find('.stream-item-header').find('a').attr('href');
                        replies.push(comment);
                    }
                    deferred.resolve(replies);
                } else {
                    deferred.reject('Error reading replies!');
                }
    });
    return deferred.promise;
}
