/**
 * Created by Administrator on 05/04/16.
 */
var async = require('async');
var Q = require('q');
var request = require('request');

exports.facebookNewsFeed = function facebookNewsFeed(companyName, fbToken, facebookAPI) {
    var deferred = Q.defer();
    var fbPostsAssociated = [];
    facebookAPI.api('/' + companyName + '/feed', {access_token: fbToken, limit: 100}, function (res) {
        var fbPosts = res.data;
        var hasNext = res.paging ? res.paging.next : false;
        if(hasNext){
            var k = 0;
            getRemainingNewsFeed(hasNext, fbPosts, k)
                .then(
                    function(success){
                        console.log(success.data.length);
                        fbPosts = fbPosts.concat(success.data);
                        async.each(fbPosts, function(fbPost, callbackFb){
                            facebookAPI.api('/' + fbPost.id + '/comments', {access_token: fbToken, order: 'reverse_chronological', limit: 100}, function (res) {
                                fbPost.comments = res.data;
                                fbPostsAssociated.push(fbPost);
                                callbackFb();
                            });
                        }, function(err){
                            if( err ) {
                                deferred.reject({error: 'Eroare la procesarea datelor de pe Facebook!'});
                            } else {
                                deferred.resolve(fbPostsAssociated);
                            }
                        });
                    },
                    function(err){
                        console.log(err);
                        deferred.reject(err);
                    }
                )
        } else {
            async.each(fbPosts, function(fbPost, callbackFb){
                facebookAPI.api('/' + fbPost.id + '/comments', {access_token: fbToken, order: 'reverse_chronological', limit: 100}, function (res) {
                    fbPost.comments = res.data;
                    fbPostsAssociated.push(fbPost);
                    callbackFb();
                });
            }, function(err){
                if( err ) {
                    deferred.reject({error: 'Eroare la procesarea datelor de pe Facebook!'});
                } else {
                    deferred.resolve(fbPostsAssociated);
                }
            });
        }
    });

    return deferred.promise;

};

function getRemainingNewsFeed(url, fbPosts, nr){
    var deferred = Q.defer();
    var newOptions = {
        uri: url,
        headers: {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'
        },
        timeout: 5 * 60 * 1000
    };
    nr++;
    request(newOptions, function(err, response, body){
        if (response.statusCode == 200) {
            var jsonResponse = JSON.parse(body);
            if(!jsonResponse.paging){
                var objectToReturn = {
                    data : fbPosts
                };
                deferred.resolve(objectToReturn);
            } else {
                fbPosts = fbPosts.concat(jsonResponse.data);
                if(jsonResponse.paging){
                    deferred.resolve(getRemainingNewsFeed(jsonResponse.paging.next, fbPosts, nr));
                } else {
                    var objectToReturn = {
                        data : fbPosts
                    };
                    deferred.resolve(objectToReturn);
                }
            }
        } else {
            console.log(body);
            deferred.reject('Error reading Facebook News Feed!');
        }
    });
    return deferred.promise;

}
