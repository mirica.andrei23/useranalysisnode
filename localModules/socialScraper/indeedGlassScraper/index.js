/**
 * Created by Administrator on 04/04/16.
 */
var indeed 		= require('./indeed_crawler');
var Q 				= require('q');
var glass = require('./glass_crawler');

exports.crawlReviews = function crawlReviews(indeedURL) {
    var deferred = Q.defer();
    var reviewsArray = [];
    var reviewError = false;
    var indeedPercentComplete = 0;

    var ID = indeed.crawlIndeedReview(indeedURL, function (percentComplete){
            indeedPercentComplete = percentComplete;
        })
        .then(function(reviews) {
            console.log(reviews.length + " indeed reviews!");
            reviewsArray = reviewsArray.concat(reviews);
        })
        .fail(function(error) {
            reviewError = true;
            deferred.reject(error);
        });

    Q.all([ID])
        .then(function() {
            if (!reviewError){
                deferred.resolve(reviewsArray);
            } else {
                deferred.reject(reviewError);
            }
        });

    return deferred.promise;

};

exports.crawlGlassReviews = function crawlGlassReviews(glassURL) {
    var deferred = Q.defer();
    var reviewsArray = [];
    var reviewError = false;
    var glassPercent = 0;

    var glassID = glass.crawlGlassdoorReview(glassURL, function (percentComplete){
            glassPercent = percentComplete;
        })
        .then(function(reviews) {
            console.log(reviews.length + " glass reviews!");
            reviewsArray = reviewsArray.concat(reviews);
        })
        .fail(function(error) {
            reviewError = true;
            deferred.reject(error);
        });

    Q.all([glassID])
        .then(function() {
            if (!reviewError){
                deferred.resolve(reviewsArray);
            } else {
                deferred.reject(reviewError);
            }
        });

    return deferred.promise;

};