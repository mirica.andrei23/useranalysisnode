/**
 * Created by Administrator on 05/04/16.
 */
var async = require('async');
var Q = require('q');
var fullContactUrl = 'https://api.fullcontact.com/v2/company/lookup.json?domain=';
var request = require('request');
var Config = require('../../config/environment.js');
var config = new Config();
var indeedScraper = require ('./indeedGlassScraper');
var linkedinscraper = require("./linkedinScraper");
var TwitterScraper = require("./twitterScraper");
var facebookFeed = require("./facebookFeed");

exports.fullProfiles = function fullProfiles(companyURL, FB, FBToken) {
    var deferred = Q.defer();
    var fullContactCall = fullContactUrl + companyURL + '&apiKey=' + config.fullContactKey;
    request(fullContactCall, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var objectToSend = {
                twitter : [],
                facebook: [],
                linkedin: []
            };
            objectToSend.companyLogo = JSON.parse(body).logo ? JSON.parse(body).logo : '';
            objectToSend.companyIntel = JSON.parse(body).organization;
            if(JSON.parse(body).socialProfiles.length > 0){
                var socialProfiles = JSON.parse(body).socialProfiles;
                var indeedObj = {
                    typeId: 'Indeed'
                };
                if(objectToSend.companyIntel.name.split(' ').length > 0){
                    indeedObj.id = companyURL.split('.')[0];
                } else {
                    indeedObj.id = objectToSend.companyIntel.name;
                }
                socialProfiles.push(indeedObj);
                async.eachSeries(socialProfiles, function(socialProfile, callback) {
                    console.log(socialProfile.typeId);
                    switch(socialProfile.typeId){
                        case 'twitter':
                            objectToSend.twitter.push({
                                url : socialProfile.url,
                                username: socialProfile.username,
                                id: socialProfile.id
                            });
                            var currentTwitterIndex = objectToSend.twitter.length - 1;
                            TwitterScraper.crawlTwitterUpdates(objectToSend.twitter[currentTwitterIndex].username, objectToSend.twitter[currentTwitterIndex].url).then(
                                function(success){
                                    objectToSend.twitter[currentTwitterIndex].newsFeed = [];
                                    objectToSend.twitter[currentTwitterIndex].newsFeed = success;
                                    callback();
                                },
                                function(err){
                                    callback(err);
                                }
                            );
                            break;
                        case 'linkedincompany':
                            if(!socialProfile.id){
                                callback();
                            } else {
                                objectToSend.linkedin.push({
                                    url : socialProfile.url,
                                    username: socialProfile.username,
                                    id: socialProfile.id
                                });
                                var currentLinkedinIndex =objectToSend.linkedin.length - 1;

                                linkedinscraper.crawlLinkedinUpdates(objectToSend.linkedin[currentLinkedinIndex].url, objectToSend.linkedin[currentLinkedinIndex].id).then(
                                    function (success) {
                                        objectToSend.linkedin[currentLinkedinIndex].newsFeed = success;
                                        callback();
                                    },
                                    function (err) {
                                        callback(err);
                                    }
                                );
                            }
                            break;
                        case 'facebook':
                            objectToSend.facebook.push({
                                url : socialProfile.url,
                                companyName : socialProfile.url.replace('https://www.facebook.com/', '')
                            });
                            var currentFacebookIndex =objectToSend.facebook.length - 1;
                            facebookFeed.facebookNewsFeed(objectToSend.facebook[currentFacebookIndex].companyName, FBToken, FB)
                                .then(
                                    function(success){
                                        objectToSend.facebook[currentFacebookIndex].newsFeed = success;
                                        callback();
                                    },
                                    function (err) {
                                        callback(err);
                                    }
                                );
                            break;
                        case 'Indeed':
                            console.log(socialProfile);
                            objectToSend.Indeed = {
                                url : 'http://www.indeed.com/cmp/' + socialProfile.id + '/reviews',
                                name: socialProfile.id
                            };
                            indeedScraper.crawlReviews(objectToSend.Indeed.url).then(
                                function (success) {
                                    objectToSend.Indeed.newsFeed = success;
                                    callback();
                                },
                                function (err) {
                                    callback(err);
                                }
                            );
                            break;
                        default:
                            callback();
                    }
                }, function(err){
                    if( err ) {
                        console.log(err);
                        deferred.reject({error: 'Eroare la procesarea profilurilor de socializare!'});
                    } else {
                        deferred.resolve(objectToSend);
                    }
                });
            } else {
                objectToSend.socials = [];
                console.log('Wrong!');
                deferred.resolve(objectToSend);
            }
        } else {
            deferred.reject({error: 'Eroare la interogare!'});
        }
    });
    return deferred.promise;

};

exports.partialProfiles = function getOnlyProfiles(companyURL){
    var deferred = Q.defer();
    var fullContactCall = fullContactUrl + companyURL + '&apiKey=' + config.fullContactKey;
    request(fullContactCall, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var objectToSend = {
                twitter : [],
                facebook: [],
                linkedin: []
            };
            objectToSend.companyLogo = JSON.parse(body).logo ? JSON.parse(body).logo : '';
            objectToSend.companyIntel = JSON.parse(body).organization;
            if(JSON.parse(body).socialProfiles.length > 0){
                var socialProfiles = JSON.parse(body).socialProfiles;
                var indeedObj = {
                    typeId: 'Indeed'
                };
                if(objectToSend.companyIntel.name.split(' ').length > 0){
                    indeedObj.id = companyURL.split('.')[0];
                } else {
                    indeedObj.id = objectToSend.companyIntel.name;
                }
                socialProfiles.push(indeedObj);
                async.eachSeries(socialProfiles, function(socialProfile, callback) {
                    console.log(socialProfile.typeId);
                    switch(socialProfile.typeId){
                        case 'twitter':
                            objectToSend.twitter.push({
                                url : socialProfile.url,
                                username: socialProfile.username,
                                id: socialProfile.id
                            });
                            callback();
                            break;
                        case 'linkedincompany':
                            objectToSend.linkedin.push({
                                url : socialProfile.url,
                                username: socialProfile.username,
                                id: socialProfile.id
                            });
                            callback();
                            break;
                        case 'facebook':
                            objectToSend.facebook.push({
                                url : socialProfile.url,
                                companyName : socialProfile.url.replace('https://www.facebook.com/', '')
                            });
                            callback();
                            break;
                        case 'Indeed':
                            objectToSend.Indeed = {
                                url : 'http://www.indeed.com/cmp/' + socialProfile.id + '/reviews',
                                name: socialProfile.id
                            };
                            callback();
                            break;
                        default:
                            callback();
                    }
                }, function(err){
                    if( err ) {
                        console.log(err);
                        deferred.reject({error: 'Eroare la procesarea profilurilor de socializare!'});
                    } else {
                        deferred.resolve(objectToSend);
                    }
                });
            } else {
                objectToSend.socials = [];
                console.log('Wrong!');
                deferred.resolve(objectToSend);
            }
        } else {
            deferred.reject({error: 'Eroare la interogare!'});
        }
    });
    return deferred.promise;

};

exports.profileIntel = function profileIntel(profileName, profileObject, FB, FBToken){
    var deferred = Q.defer();
    switch (profileName){
        case 'twitter':
            TwitterScraper.crawlTwitterUpdates(profileObject.username, profileObject.url).then(
                function(success){
                    profileObject.newsFeed = [];
                    profileObject.newsFeed = success;
                    deferred.resolve(profileObject);
                },
                function(err){
                    deferred.reject(err);
                }
            );
            break;
        case 'linkedincompany':
            if(!profileObject.id){
                deferred.resolve(profileObject);
            } else {
                linkedinscraper.crawlLinkedinUpdates(profileObject.url, profileObject.id).then(
                    function (success) {
                        profileObject.newsFeed = success;
                        deferred.resolve(profileObject);
                    },
                    function (err) {
                        deferred.reject(err);
                    }
                );
            }
            break;
        case 'facebook':
            facebookFeed.facebookNewsFeed(profileObject.companyName, FBToken, FB)
                .then(
                    function(success){
                        profileObject.newsFeed = success;
                        deferred.resolve(profileObject);
                    },
                    function (err) {
                        deferred.reject(err);
                    }
                );
            break;
        case 'Indeed':
            indeedScraper.crawlReviews(profileObject.url).then(
                function (success) {
                    profileObject.newsFeed = success;
                    deferred.resolve(profileObject);
                },
                function (err) {
                    deferred.reject(err);
                }
            );
            break;
        default:
            deferred.resolve(profileObject);
    }

    return deferred.promise;
};