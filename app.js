var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoose = require('mongoose');
var Config = require('./config/environment.js'),
    config = new Config();
var passport = require('passport');
var email = require('node-mandrill')("eDfR8OnuNJS4VTvYkwnhyQ");
var morgan       = require('morgan');
var logger = require('./config/winston');

var sessionSecret = "superDuperF!!.inS3crET";

var FullContact = require('fullcontact').createClient(config.fullContactKey);
var ig = require('instagram-node').instagram();
var AlchemyAPI = require('alchemy-api');
var alchemy = new AlchemyAPI(config.alchemyKey);

var Twitter = require('twitter');
var clientTwitter = new Twitter({
  consumer_key: config.twitter.consumer_key,
  consumer_secret: config.twitter.consumer_secret,
  access_token_key: config.twitter.access_token_key,
  access_token_secret: config.twitter.access_token_secret
});

var Linkedin = require('node-linkedin')(config.linkedin.appId, config.linkedin.appSecret);
//set callback param in order to get access token
var db = require('./config/db');
var cors = require('cors');
mongoose.connect(db.url);

var app = express();
var timeout = require('connect-timeout');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(timeout(10000000));
app.use(haltOnTimedout);

function haltOnTimedout(req, res, next){
  if (!req.timedout) next();
}
// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public/assets/', 'icon-sentiment.ico')));
app.use(morgan('dev'));
app.use(morgan({ "stream": logger.stream }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: sessionSecret, resave: false, saveUninitialized: false })); // session secret

var FB = require('fb');
require('./config/passport')(passport, logger); // pass passport for configuration
require('./app/routes.js')(app, email, logger, passport, FB, config); // configure our routes
require('./app/globalAPI.js')(app, email, logger, express.Router()); // configure our routes
app.use(cors());
require('./app/mainApi')(app, config, sessionSecret, logger, express.Router(), FullContact, alchemy, Linkedin, FB, clientTwitter); // load our private routes and pass in our app and session secret

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(req, res, next){
  res.setTimeout(360000, function(){
    console.log('Request has timed out.');
    res.send(408);
  });

  next();
});
// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    console.log(err);
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
