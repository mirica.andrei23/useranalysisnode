var mongoose = require('mongoose');
var User = require('./models/user');
var XRegExp  = require('xregexp').XRegExp;
var validator = require('validator');
var crypto   = require('crypto');

module.exports = function(app, mandrill, logger, router) {

    //access control allow origin *
    app.all("/globalAPI/*", function (req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
        res.setHeader("Access-Control-Allow-Credentials", false);
        res.setHeader("Access-Control-Max-Age", '86400'); // 24 hours
        res.setHeader("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization");
        next();
    });

    router.route('/getClients')
        .get(function(req,res){
            Clients.find({}).exec(function(err,resp){
                if(err)
                    res.json(err);
                else
                    res.json(resp);
            })
        });
    router.route('/getDepartments')
        .get(function(req,res){
            Departments.find({}).exec(function(err,resp){
                if(err)
                    res.json(err);
                else
                    res.json(resp);
            })
        });
    router.route('/getRoles')
        .get(function(req,res){
            Roles.find({}).exec(function(err,resp){
                if(err)
                    res.json(err);
                else
                    res.json(resp);
            })
        });

    router.route('/createAccount')
        .post(function (req, res) {
            var namePatt = new XRegExp('^[a-zA-Z]{3,100}$');

            var firstname = req.body.firstname?req.body.firstname:"";
            var lastname = req.body.lastname?req.body.lastname:"";
            var email = req.body.email?req.body.email:"";
            var password = req.body.password?req.body.password:"";

            var info = {error: true, type:"danger"};

            //validate data
            if(!validator.isEmail(email)){
                info.message = "Adresa de e-mail nu este valida";
                res.json(info);
            }else if(!namePatt.test(firstname.replace(/ /g,'').replace(/-/g,'').replace(/\./g,''))){
                info.message = "Prenumele trebuie sa contina minim 3 litere si doar caracterele speciale '-', '.'";
                res.json(info);
            }
            else if(!namePatt.test(lastname.replace(/ /g,'').replace(/-/g,'').replace(/\./g,''))){
                info.message = "Numele trebuie sa contina minim 3 litere si doar caracterele speciale '-', '.'";
                res.json(info);
            }
            else if(password.length < 6 || password.length > 32){
                info.message = "Parola trebuie sa contina intre 6 si 32 de caractere";
                res.json(info);
            }else{
                //data is valid
                User.findOne({username: {$regex: "^"+email.replace(/\+/g,"\\+")+"$", $options: "i"}}, function(err, user) {
                    // if there are any errors, return the error
                    if (err){
                        res.json(err);
                    }else if (user) {
                        // check to see if there's already a user with that email
                        info.message = "Acest e-mail este deja folosit";
                        res.send(info);
                    } else {
                        // create the user
                        var newUser = new User();
                        if(req.body.client)
                            newUser.id_client = req.body.client;
                        else
                            newUser.id_client = "";
                        newUser.username = email;
                        newUser.firstname     = firstname;
                        newUser.lastname     = lastname;
                        newUser.password = newUser.generateHash(password);
                        newUser.enabled = false; //enable only after email activation
                        newUser.last_updated = Date.now();
                        newUser.phone="";
                        newUser.id_department="";
                        newUser.id_role = "";
                        //set activation token
                        crypto.randomBytes(40, function(err, buf) {
                            if(err){
                                res.send(err);
                            }else{
                                newUser.activationToken = buf.toString('hex');

                                //save user
                                newUser.save(function(err, inserted) {
                                    if (err){
                                        res.send(err);
                                    }else{
                                        //we need to email the user
                                        //first, create an activation link
                                        var activationLink;
                                        //if the account was created from Staywell site, create a special link

                                        activationLink = 'http://' + req.headers.host + '/activateAccount/' + inserted.activationToken;
                                        mandrill({from: 'adminQServiceDesk@qualitance.ro',
                                            to: [inserted.username],
                                            subject:'Activare cont QServiceDesk',
                                            text:
                                            'Draga '+inserted.name+',\n\n\n'+
                                            'Ati primit acest email deoarece v-ati inregistrat pe QServiceDesk.\n\n' +
                                            'Va rugam accesati link-ul de mai jos (sau copiati link-ul in browser) pentru validarea adresei de e-mail:\n\n' +
                                            activationLink + '\n\n' +
                                            'Daca nu v-ati creat cont pe QServiceDesk, va rugam sa ignorati acest e-mail\n\n\n'+
                                            'Toate cele bune,\nEchipa QServiceDesk'
                                        }, function(errm){
                                            if(errm) {
                                                logger.error(errm);
                                                res.send(errm);
                                            }else{
                                                info.error = false;
                                                info.type = "success";
                                                info.message = "Un email de verificare a fost trimis";
                                                info.user = email;
                                                res.json(info);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    app.use('/globalAPI', router);
};