/**
 * Created by Andrew on 29-Apr-16.
 */
var mongoose = require('mongoose');
var Schema			= mongoose.Schema;

// define the schema for our user model
var socialSchema = new Schema({
    companyName: {type: String},
    comapnyWebsite: {type: String},
    logoImage: String,
    last_updated: Date,
    moreInfo: Object
});

// create the model for users and expose it to our app
module.exports = mongoose.model('socialProfiles', socialSchema, 'socialProfiles');