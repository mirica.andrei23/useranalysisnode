/**
 * Created by Andrew on 29-Apr-16.
 */
var mongoose = require('mongoose');
var crypto   = require('crypto-js/sha256');
var Schema			= mongoose.Schema;

// define the schema for our user model
var userSchema = new Schema({
    account_expired: {type: Boolean, select: false},
    account_locked: {type: Boolean, select: false},
    enabled: {type: Boolean, select: false},
    image_path: String,
    job: String,
    last_updated: Date,
    name: String,
    phone: {type: String},
    birthday: {type: Date},
    password     : {type: String, select: false},
    username     : {type: String, index: true},
    socialProfilesID: [{type: Schema.Types.ObjectId, ref: 'socialProfiles'}],
    resetPasswordToken: {type: String, select: false},
    resetPasswordExpires: {type: Date, select: false},
    activationToken: {type: String, select: false}
});

userSchema.statics.findAndModify = function (query, sort, doc, options, callback) {
    return this.collection.findAndModify(query, sort, doc, options, callback);
};

// generating a hash
userSchema.methods.generateHash = function(password) {
    return crypto(password).toString();
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return crypto(password).toString()===this.password;
};

userSchema.methods.getEmailTitle = function () {
    switch(this.title){
        case 1: return "D-le";
        case 2: return "D-na";
        case 3: return "Prof.";
        case 4: return "Dr.";
        default: return "";
    }
};

// create the model for users and expose it to our app
module.exports = mongoose.model('users', userSchema, 'users');