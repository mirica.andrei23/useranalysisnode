var Users = require('./models/user');
var crypto   = require('crypto');

module.exports = function(app, email, logger, passport, FB, Config) {

    // server routes ===========================================================
    // handle things like api calls
    // authentication routes

    // frontend routes =========================================================
    // route to handle all angular requests
    app.all("/*", function(req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "http://localhost:8082");
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
        res.setHeader("Access-Control-Allow-Credentials", false);
        res.setHeader("Access-Control-Max-Age", '86400'); // 24 hours
        res.setHeader("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept");
        next();
        // just move on to the next route handler
    });

    app.get('/', function(req, res) {
        if(req.isAuthenticated()){
            res.redirect('/authenticated');
        }else {
            if(req.session.requestedActivation){
                delete req.session.requestedActivation;
                res.render('confirmActive.html');
            }
            else {
                FB.api('oauth/access_token', {
                    client_id: Config.facebook.appId,
                    client_secret: Config.facebook.appSecret,
                    grant_type: 'client_credentials'
                }, function (response) {
                    if(!response || response.error) {
                        console.log(!response ? 'error occurred' : response.error);
                        res.status(500).send({error: 'Eroare la obtinerea token-ului de Facebook!'});
                    } else {
                        req.session.FBaccessToken = response.access_token;
                        FB.setAccessToken(req.session.FBaccessToken);
                        console.log(req.session.FBaccessToken);
                        res.render('outside/main.ejs', {root: 'views'}); // load our public/index.html file
                    }
                });
            }
        }
    });
    app.get('/signup', function(req, res) {
        res.sendFile('signup.html',{root: 'public'}); // load our public/index.html file
    });
    app.get('/activateAccount/:token', function (req, res) {
        //put a requestedActivation variable on this session
        //this is used for marking that an info modal needs
        //to be displayed after redirecting to the home page
        req.session.requestedActivation = true;
        Users.findOne({ activationToken: req.params.token}, function(err, user) {
            if (err || !user) {
                logger.error(err);
                res.redirect('/');
            }else{
                user.enabled = true;
                user.activationToken = null;
                user.save(function (err, user) {
                    if(err){
                        logger.error(err);
                    }else{
                        req.session.accountActivated = true;
                    }
                    res.render('confirmActive.html');
                });
            }
        });
    });
    app.post('/login', function (req, res, next) {
        //middleware to allow flashing messages on empty user/password fields
        if(!req.body.email || !req.body.password){
            return res.send({error: true, message: 'Campurile sunt obligatorii'});
        }else{
            passport.authenticate('local-login', function (err, user, info) {
                if(err){

                    logger.log(err);
                    return res.send({error: true, message: "A aparut o eroare pe server"});
                }else if(!user){
                    return res.send(info);
                }else{
                    req.logIn(user, function (err) {
                        if(err){
                            return res.send({error: true, message: "A aparut o eroare pe server"});
                        }else{
                            if(user.enabled === true){
                                    return  res.send({error: false, accepted: true});

                            }else if(user.enabled === false){
                                return res.send({error: false, accepted: false});
                            }else{
                                //this final else should never be reached
                                req.logout();
                            }
                        }
                    })
                }
            })(req, res, next);
        }
    });
    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();
        else
            res.redirect('/');
    }
    //redirect user to home page and open login
    app.get('/login', function (req, res) {
        req.session.showLogin = true;
        res.redirect('/');
    });
    var sendUserHome = function (req, res) {
        Users.findOne({_id: req.user._id}).exec(function (err, user) {
            if (err) {
                req.logout();
                res.redirect('/');
            } else {
                if (user.enabled === true) {
                    if(user.id_client === "")
                    {
                        res.render("insideViews/home.html", {user: req.user});
                    }
                    else
                        res.render("home.html", {user: req.user});
                } else {
                    req.logout();
                    res.redirect('/');
                }
            }
        });
    };
    app.get('/authenticated', isLoggedIn, function(req, res) {
        sendUserHome(req, res);
    });
    app.get('/logout', function(req, res) {
        req.logout();
        req.session.destroy();
        //encrypt sid using session secret
        res.redirect('/');
    });
};