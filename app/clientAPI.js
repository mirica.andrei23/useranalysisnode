/**
 * Created by Andrew on 18.04.2015.
 */
var mongoose = require('mongoose');
var User = require('./models/users');
var Roles=require('./models/roles');
var Departments = require('./models/departments');
var Clients = require('./models/clients');
var Comments = require('./models/comments');
var Tickets = require('./models/tickets');
var XRegExp  = require('xregexp').XRegExp;
var validator = require('validator');
var crypto   = require('crypto');
var async = require('async');
var _ = require('underscore');
var Severity = require('./models/severity');
var Status = require('./models/status');


module.exports = function(app, mandrill, logger, router) {

    //access control allow origin *
    app.all("/globalAPI/*", function (req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
        res.setHeader("Access-Control-Allow-Credentials", false);
        res.setHeader("Access-Control-Max-Age", '86400'); // 24 hours
        res.setHeader("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization");
        next();
    });

    router.route('/getAllSeverities')
        .get(function(req,res){
            Severity.find({}).exec(function(err,resp){
                if(err)
                    res.json(err);
                else
                    res.json(resp);
            })
        });
    router.route('/createTicket')
        .post(function(req,res){
            var ticket = new Tickets();
            var receivedTicket = req.body.newTicket;
            ticket.title= receivedTicket.title;
            ticket.content = receivedTicket.content;
            ticket.date_created = new Date();
            ticket.last_updated = new Date();
            ticket.is_solved=false;
            ticket.id_severity= receivedTicket.id_severity;
            Status.find({name: "Unassigned"}).exec(function(err,response){
                if(err)
                {
                    res.json(err);
                }
                else {
                    ticket.id_status = mongoose.Types.ObjectId(response[0]._id);
                    ticket.save(function(err,inserted){
                        if(err)
                            res.json(err);
                        else
                            User.update({_id: req.user._id}, {$addToSet: {tickets_id: inserted._id}}, {multi: false}, function (err, wRes) {
                                if(err){
                                    res.json(err);
                                }
                                else
                                    res.json({message: 'Ticked created successfully!'})
                            });
                    })
                }
            });
        });

    router.route('/userProfile')
        .get(function(req,res){
            User.find({_id: mongoose.Types.ObjectId(req.user._id)}).select('username first_name last_name background_photo profile_photo phone').populate('id_department id_role').exec(function(err,resp){
                if(err)
                    res.json(err);
                else
                    res.json(resp[0]);
            })
        })
        .post(function(req,res){
            User.update({_id: mongoose.Types.ObjectId(req.user._id)},{ $set: { first_name: req.body.firstname, last_name: req.body.lastname, phone: req.body.phone}},{multi: false},function(err,resp){
                if(err)
                    res.json(err);
                else
                    res.json({message: 'Update successful!'});
            })
        });

    router.route('/getMyTickets')
        .get(function(req,res){
            var objectWithTickets = {};
            User.find({_id: req.user._id}).deepPopulate('tickets_id tickets_id.id_status').exec(function(err,resp){
                if (err){
                    res.json(err);
                } else {
                    var tickets = resp[0].tickets_id;
                    if(tickets.length == 0){
                        res.json(objectWithTickets);
                    } else {
                        Status.find({}).exec(function(err, statuses){
                            if (err)
                                res.json(err);
                            else
                            {
                                async.each(statuses, function (item, callback) {
                                    objectWithTickets[item.name] = tickets.filter(function(ticket){
                                        if(ticket.id_status)
                                        {
                                            return ticket.id_status._id.toString() == item._id.toString();
                                        }
                                    });
                                    callback();
                                }, function (err) {
                                    if(err){
                                        res.send({error: true, message: "Eroare la cautare!"});
                                    }else{
                                        console.log(objectWithTickets);
                                        res.send(objectWithTickets);
                                    }
                                });
                            }
                        })
                    }
                }
            });
        });


    router.route('/getTicket')
        .get(function(req,res){
            Tickets.find({_id: req.query.idTicket}).deepPopulate('comments comments.id_user id_severity id_status id_users').exec(function(err,resp){
                if(err)
                    res.json(err);
                else{
                    var objectToSend = {};
                    objectToSend.data = resp[0];
                    objectToSend.userId = req.user._id;
                    res.send(objectToSend);
                }
            })
        });

    router.route('/sendMail')
        .post(function(req,res){
            var info = {};
            var emailContent = req.body;
            console.log(req.user);
            mandrill('/messages/send', {
                message: {
                    to: [{email: 'andrei.mirica@qualitance.com', name: 'Support Service Desk'}],
                    from_email: req.user.username,
                    subject: emailContent.subject,
                    text: emailContent.message
                }
            }, function(errm,response){
                if(errm) {
                    logger.error(errm);
                    res.send(errm);
                }else{
                    info.error = false;
                    info.type = "success";
                    info.message = "Email-ul a fost trimis cu succes catre echipa de suport!";
                    res.json(info);
                }
            });
        });

    router.route('/comment')
        .post(function(req,res){
            var comm = new Comments;
            comm.content = req.body.content;
            comm.id_user = req.user._id;
            comm.id_ticket = req.body.idTicket;
            comm.date_created = new Date();
            comm.save(function(err,inserted){
                if(err)
                    res.json({error:err});
                else{
                    Tickets.update({_id: req.body.idTicket},{ $push: { comments: inserted._id}},{multi: false},function(err,resp){
                        if(err)
                            res.json(err);
                        else
                        {
                            res.json({message: 'Comment added successfully!'});
                        }
                    })
                }
            });
        })
        .put(function(req,res){
            Comments.update({_id: req.query.idComment},{ $set: { content: req.body.newContent}},{multi: false},function(err,resp){
                if(err)
                    res.json(err);
                else
                {
                    res.json({message: 'Comment updated successfully!'});
                }
            })
        })
        .delete(function(req,res){
            Comments.remove({_id: req.query.id}, function (err, count) {
                if(err){
                    res.send({error: true, message: "Eroare la stergerea comentariului"});
                }else{
                    res.send({error: false, message: "Comentariul a fost sters cu succes!"});
                }
            });
        });

    app.use('/clientAPI', router);
};
