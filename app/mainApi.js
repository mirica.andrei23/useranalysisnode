var mongoose = require('mongoose');
var async = require('async');
var request = require('request');
var fs = require('fs');
var Q = require('q');
var graph = require('fbgraph');
var underscore = require('underscore');
var demoCall = 'https://api.fullcontact.com/v2/company/lookup.json?domain=qualitance.com&apiKey=aa537fa946628154';
var socialScraper = require('../localModules/socialScraper');
var lodash = require('lodash');
var redirect_uri = 'http://localhost:3000/oauth/instagram';
var alchmyAnalysis = require('../localModules/alchemyTextAnalysis');

/* GET home page. */
module.exports = function(app, Config, sessionSecret, logger, router, FullContact, alchemy, Linkedin, FB, twitterAPI) {
    router.route('/oauth/linkedin')
        .get(function(req,res){
            Linkedin.auth.getAccessToken(res, req.query.code, req.query.state, function(err, results) {
                if ( err )
                    res.status(500).send({error: 'Eroare la obtinerea token-ului de Linkedin!'});
                else {
                    req.session.linkedinToken = results.access_token;
                    console.log(req.session.linkedinToken);
                    res.redirect('/');
                }
            });
        });

    router.route('/oauth/instagram')
        .get(function(req,res){
            ig.authorize_user(req.query.code, redirect_uri, function(err, result) {
                if (err) {
                    console.log(err.body);
                    res.json("Didn't work");
                } else {
                    req.session.instagramToken = result.access_token;
                    console.log(req.session.instagramToken);
                    res.redirect('/');
                }
            });
        });

    router.get('/findBrandBySite', function(req, res, next) {
        var companyDomain = req.query.companyDomain;
        req.connection.setTimeout(20000000000);
        socialScraper.fullProfiles(companyDomain, FB, req.session.FBaccessToken)
            .then(
                function (success) {
                    res.json(success);
                },
                function (err) {
                    res.status(500).send({error: err});
                }
            )
    });

    router.get('/findBrandByName', function(req, res, next) {
        res.render('outside/main.ejs', { title: 'Express' });
    });

    router.get('/allSentiments', function(req, res, next) {
        res.render('outside/main.ejs', { title: 'Express' });
    });

    router.get('/sentimentByProfile', function(req, res, next) {
        res.render('outside/main.ejs', { title: 'Express' });
    });

    app.use('/mainApi', router);
};