var app = angular.module('app',
    [
        'ui.router',
        'controllers',
        'services',
        'ui.bootstrap',
        'ngAnimate',
        'ngCookies',
        'angulartics',
        'angulartics.google.analytics',
        'offClick'
    ]);

app.config(['$controllerProvider', '$filterProvider', function ($controllerProvider, $filterProvider) {
    app.controllerProvider = $controllerProvider;
    app.filterProvider = $filterProvider;
}]);

app.config(['$locationProvider', function($location) {
    $location.hashPrefix('!');
}]);

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('errorRecover');
}]);

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/");
    $stateProvider
        .state('home',{
            url: '/',
            templateUrl: 'partials/inside/home.ejs',
            controller: 'Home'
        })
}]);

app.run(
    [            '$rootScope', '$state', '$stateParams', '$modal','$sce',
        function ($rootScope,   $state,   $stateParams,   $modal,  $sce) {

            // It's very handy to add references to $state and $stateParams to the $rootScope
            // so that you can access them from any scope within your applications.For example,
            // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
            // to active whenever 'contacts.list' or one of its decendents is active.
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            //amazon service paths
            $rootScope.textToSearch="";

            //state events
            $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams){
                    if($modalStack)
                        $modalStack.dismissAll();
                });
        }
    ]
);