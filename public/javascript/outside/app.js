var app = angular.module('app', [
    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'ngAnimate',
    'controllers',
    'services',
    'ngSanitize',
    'offClick',
    'ngMaterial',
    'ngMdIcons',
    'jsonFormatter',
    'LocalStorageModule',
    'fullPage.js'
]);

app.config(['$controllerProvider', '$filterProvider', function ($controllerProvider, $filterProvider) {
    app.controllerProvider = $controllerProvider;
    app.filterProvider = $filterProvider;
}]);

app.config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('!');
}]);

app.config(function (localStorageServiceProvider) {
    localStorageServiceProvider
        .setPrefix('socialScraper')
        .setStorageType('localStorage');
});

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise(function ($injector, $location) {
            $location.path("/home");
    });
    $stateProvider
        .state('home',{
            url: '/home',
            templateUrl: 'partials/outside/mainResults.html',
            controller: 'searchController'
        })
}]);

app.run(
    [            '$rootScope', '$state', '$stateParams', '$modal', '$sce', '$location', '$ocLazyLoad',
        function ($rootScope,   $state,   $stateParams,   $modal,   $sce ,   $location,   $ocLazyLoad) {

            // It's very handy to add references to $state and $stateParams to the $rootScope
            // so that you can access them from any scope within your applications.For example,
            // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
            // to active whenever 'contacts.list' or one of its decendents is active.
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.textToSearch="";

            //state events
            $rootScope.$on('$stateChangeSuccess',
                function(){
                    window.scrollTo(0,0);
                });

        }
    ]
);