/**
 * Created by Andrew on 19-Mar-16.
 */
controllers.controller('searchController', ['$scope', '$rootScope', 'RootService', function ($scope, $rootScope, RootService, localStorageService) {

    $rootScope.searchForBrand = function () {
        RootService.brandSocials.query({companyDomain: $scope.website}).$promise.then(function(resp){
            $scope.jsonData = resp;
            localStorageService.set(new Date(), $scope.jsonData);
        })
            .catch(function(err){
                console.log(err);
            })
    };

}]);