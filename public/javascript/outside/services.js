var services = angular.module('services', ['ngResource']);

services.factory('RootService', ['$resource', function ($resource) {
    return {
        brandSocials: $resource('mainApi/findBrandBySite', {}, {
            query: { method: 'GET', isArray: false }
        })
    }
}]);